package service

import (
	"fmt"
	"net/http"
	"reflect"
	"regexp"

	"dennis.com/GoGin/model"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type Users struct {
	Users    []model.User `json:"users" binding:"gt=0,lt=6,dive"` // 0 < users長度 < 6 , tag有先後順序的區別前面先判，短路原則，且dive(深入去看User的tag )需放最後
	SliceLen int          `json:"sliceLen" binding:"usersLen"`
}

// // 判斷 users的SliceLen有沒有寫對
func UsersValidate(fl validator.FieldLevel) bool {
	if usersData, ok := fl.Parent().Interface().(Users); ok {
		fmt.Println(len(usersData.Users), usersData.SliceLen)
		if len(usersData.Users) == usersData.SliceLen {
			return true
		} else {
			return false
		}
	}
	return false
}

// Customer bind validate tag
// v9以上的寫法
func MyCustomRuleV9Up(fl validator.FieldLevel) bool {
	// 類型斷言 value, ok := x.(T)，以下斷言f1是否可轉成uint
	if fieldData, ok := fl.Field().Interface().(uint); ok {
		fmt.Printf("fl = %v, type = %T\n", fieldData, fieldData)
		// 正則表達式
		if res, _ := regexp.MatchString("p([a-z]+)ch", fmt.Sprintf("%v", fieldData)); res {
			return true
		}
	}
	return true
}

// Customer bind validate tag
// v8寫法，這邊用的是v10，故不能用
func MyCustomRuleV8(v *validator.Validate, topStruct reflect.Value, currentStructOrField reflect.Value,
	field reflect.Value, fieldType reflect.Type, fieldKind reflect.Kind, param string) bool {
	fmt.Println(v)
	fmt.Println(topStruct)
	fmt.Println(currentStructOrField)
	fmt.Println(field)
	fmt.Println(fieldType)
	fmt.Println(fieldKind)
	fmt.Println(param)
	return false
}

func MustLogin() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		token := ctx.Query("token")
		if token == "" {
			// 沒有token
			ctx.String(http.StatusUnauthorized, "沒有token")
			ctx.Abort() // 不往下走
		} else {
			// 驗證token，這裡先假設所有token都通過
			// ctx.Next() // 繼續往下走，可以不寫
			fmt.Printf("MustLogin token: %v\n", token)
		}
	}
}

// 路由格式: GET "http://localhost:8005/v2/anything(except "users")"
func JustTest(ctx *gin.Context) {
	something := ctx.Param("test")
	ctx.JSON(
		200, gin.H{
			"success": something,
		})
}

// 路由格式: GET "http://localhost:8005/v2/users?username=Jack" or "http://localhost:8005/v2/users"
func GetUsers(ctx *gin.Context) {
	queryuser := ctx.DefaultQuery("username", "")
	if queryuser == "" {
		users, err := model.MyUserDao.GetAllUsers()
		if err != nil {
			ctx.String(http.StatusNotAcceptable, "errMsg = "+err.Error())
			return
		}
		// ctx.String(200, fmt.Sprintf("%v", users))
		ctx.JSON(200, users)
		return
	} else {
		user, err := model.MyUserDao.GetUserByUserName(queryuser)
		if err != nil {
			ctx.String(http.StatusNotAcceptable, "errMsg = "+err.Error())
			return
		}
		ctx.JSON(
			200, gin.H{
				"success": user,
			})
	}
}

// 路由格式: POST "http://localhost:8005/v2/user?token=xxx"
// 此時已登入
func AddUser(ctx *gin.Context) {
	newuser := model.User{}

	// // 資料為url附帶的query參數，透過User{} field mapping，轉成User{} object
	// if err := ctx.BindQuery(&newuser); err != nil {
	// 	ctx.String(http.StatusBadRequest, "AddUser err:"+err.Error())
	// 	ctx.Abort()
	// }

	// 資料為POST body裡的資料，透過User{} field mapping，轉成User{} object
	if err := ctx.BindJSON(&newuser); err != nil {
		ctx.String(http.StatusBadRequest, "AddUser err:"+err.Error())
		return
	}
	ctx.JSON(200, newuser)
}

// 路由格式: POST "http://localhost:8005/v2/muser?token=xxx"
// 此時已登入
func AddMUser(ctx *gin.Context) {
	newusers := Users{}

	// // 資料為url附帶的query參數，透過User{} field mapping，轉成User{} object
	// if err := ctx.BindQuery(&newuser); err != nil {
	// 	ctx.String(http.StatusBadRequest, "AddUser err:"+err.Error())
	// 	ctx.Abort()
	// }

	// 資料為POST body裡的資料，透過User{} field mapping，轉成User{} object
	if err := ctx.BindJSON(&newusers); err != nil {
		ctx.String(http.StatusBadRequest, "AddMUser err:"+err.Error())
		return
	}
	ctx.JSON(200, newusers)
}

// 路由格式: DEL "http://localhost:8005/v2/user?token=xxx&username=Jack"
// 此時已登入
func DelUser(ctx *gin.Context) {
	ctx.String(200, "DelUser")
}
