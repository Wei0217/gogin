package model

import "encoding/json"

// username=xxx&pwd=xxx&userid=xxx
// form tag是query時，對應的id
type User struct {
	UserId   uint   `json:"userId" form:"userid" binding:"omitempty,mycustomrule"`               // jsonMapping
	UserName string `json:"userName" form:"username" binding:"required,min=8,max=14"`            // jsonMapping		binding:"規則"，表示此field在bindXX()時必須符合規則，否則err
	UserPwd  string `json:"userPwd" form:"pwd" binding:"required,min=8,max=14,nefield=UserName"` // jsonMapping		binding:"規則"()時必須有該參數
}

// https://pkg.go.dev/github.com/go-playground/validator/v10#section-readme
/* binding用法:
binding:"規則"，表示此field在bindXX()時必須符合規則，否則err
"required"	不能沒有該field，也不能為 0 或 ""
"nefield=AnotherFieldName"	不能與某field依樣
"min=4"	field值 >= 4
"max=9"	field值 <= 9
"ipv4"	網路是用ipv4
mycustomrule	也可自定義驗證規則
ex binding:"required,min=4,max=9,nefield=UserName,ipv4"
ex binding:"omitempty,gt=5"	搭配用，可以不填該參數，默認值就是0，但如果有填，那必須>5
*/

func NewUser(uId uint, uName string, uPwd string) *User {
	newUser := User{uId, uName, uPwd}
	return &newUser
}

func (this *User) Serialize() ([]byte, error) {
	return json.Marshal(this)
}

func DeserializeUser(data []byte) (*User, error) {
	var userData User
	err := json.Unmarshal(data, &userData)
	if err != nil {
		return nil, err
	}
	return &userData, nil
}
