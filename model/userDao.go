package model

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/gomodule/redigo/redis"
)

var (
	MyUserDao *UserDao
)

type UserDao struct {
	pool *redis.Pool
}

func NewUserDao(myPool *redis.Pool) *UserDao {
	return &UserDao{pool: myPool}
}

func (this *UserDao) GetUserByUserName(userName string) (user *User, err error) {
	conn := this.pool.Get()
	defer conn.Close()
	res, err := redis.String(conn.Do("HGet", "users", userName))
	if err != nil {
		if err == redis.ErrNil {
			err = ERROR_USER_NOTEXIST
		}
		return
	}

	user = &User{}
	err = json.Unmarshal([]byte(res), user)
	if err != nil {
		fmt.Println("Dao GetUserByUserName Unmarshal err =", err)
		return
	}
	return
}

func (this *UserDao) GetAllUsers() (users *[]User, err error) {
	conn := this.pool.Get()
	defer conn.Close()
	res, err := redis.Strings(conn.Do("HGetAll", "users"))
	if err != nil {
		fmt.Println(err)
		if err == redis.ErrNil {
			err = ERROR_USER_NOTEXIST
		}
		return
	}

	users = &[]User{}
	fmt.Println(res)
	for i := 0; i < len(res); i++ {
		if i%2 == 1 {
			fmt.Printf("%v\n", res[i])
			user := &User{}
			err = json.Unmarshal([]byte(res[i]), user)
			if err != nil {
				fmt.Println("Dao GetAllUsers Unmarshal err =", err)
				return
			}
			(*users) = append((*users), *user)
		}
	}

	fmt.Println(users)
	return
}

func (this *UserDao) Login(userName string, userPwd string) (user *User, err error) {
	user, err = this.GetUserByUserName(userName)
	if err != nil {
		fmt.Println("Dao Login GetUserById err =", err)
		return
	}
	fmt.Printf("user.UserPwd = %q, userPwd = %q\n", user.UserPwd, userPwd)
	if user.UserPwd != userPwd {
		err = ERROR_USER_PWDINCORRECT
		return
	}
	return
}

func (this *UserDao) Register(userId uint, userName string, userPwd string) (user *User, err error) {
	conn := this.pool.Get()
	defer conn.Close()
	if strings.ToLower(userName) == "users" {
		err = ERROR_USER_ILLEGALUSERNAME
		return
	}
	_, err = this.GetUserByUserName(userName)
	if err == nil {
		err = ERROR_USER_EXIST
		return
	}

	user = &User{userId, userName, userPwd}
	data, err := user.Serialize()
	if err != nil {
		fmt.Println("Dao Register Serialize err =", err)
		return
	}
	reply, err := conn.Do("HSetnx", "users", userName, data)

	fmt.Printf("reply = %q, err = %q, data = %q\n", reply, err, data)

	if err != nil {
		fmt.Println("Dao Register redis err =", err)
		return
	}
	return
}

func (this *UserDao) CreateSomeUser() (err error) {
	conn := this.pool.Get()
	defer conn.Close()

	reply, err := conn.Do("HSet", "users",
		"Dennis", "{\"userId\":0,\"userName\":\"Dennis\",\"userPwd\":\"123\"}",
		"Mike", "{\"userId\":0,\"userName\":\"Mike\",\"userPwd\":\"456\"}",
		"Mary", "{\"userId\":0,\"userName\":\"Mary\",\"userPwd\":\"789\"}")

	fmt.Printf("reply = %q, type = %T, err = %q\n", reply, reply, err) // reply => int64

	if err != nil {
		fmt.Println("Dao CreateSomeUser redis err =", err)
		return
	}
	return
}

func TrimNewline(str string) string {
	str = strings.ReplaceAll(str, "\r", "")
	str = strings.ReplaceAll(str, "\n", "")
	str = strings.ReplaceAll(str, " ", "")
	return str
}
