package model

import (
	"time"

	"github.com/gomodule/redigo/redis"
)

var MyPool *redis.Pool

func InitPool(addr string, maxIdle int, maxActive int, idleTimeout time.Duration) {
	MyPool = &redis.Pool{
		MaxIdle:     maxIdle,     // 表示和redis的最初的連接數
		MaxActive:   maxActive,   // 表示和redis的最大連接數，0表示無限制
		IdleTimeout: idleTimeout, // 最大空閒時間
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", addr) // redis.Dial("tcp", "localhost:6379")
		},
	}
}

// InitPool first
func InitUserDao() {
	MyUserDao = NewUserDao(MyPool)
}
