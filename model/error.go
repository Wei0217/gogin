package model

import "errors"

var (
	ERROR_USER_NOTEXIST        = errors.New("ERR 用戶不存在...")
	ERROR_USER_EXIST           = errors.New("ERR 用戶已存在...")
	ERROR_USER_PWDINCORRECT    = errors.New("ERR 密碼錯誤...")
	ERROR_USER_ILLEGALUSERNAME = errors.New("ERR 非法用戶名...")
)
