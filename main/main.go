package main

import (
	"net/http"
	"strconv"
	"time"

	"dennis.com/GoGin/model"
	"dennis.com/GoGin/service"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

func init() {
	// 初始化 redis pool
	// model.InitPool("GoGinRedis:6379", 16, 0, 300*time.Second)
	model.InitPool("localhost:6379", 16, 0, 300*time.Second)
	model.InitUserDao()
}

func main() {
	router := gin.Default()

	// 註冊綁定 自定義validate tag 與 自定義驗證方法
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		v.RegisterValidation("mycustomrule", service.MyCustomRuleV9Up)
	}
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		v.RegisterValidation("usersLen", service.UsersValidate)
	}

	{
		router.GET("/ping", func(ctx *gin.Context) {
			// ctx.JSON(httpstatus, struct obj)
			ctx.JSON(
				200, gin.H{
					"message": "ping",
				})
		})
		// 路由格式: "http://localhost:8005/ping/:key1/:key2/:key3"
		// 取得key值: ctx.Param("key值")
		router.POST("/ping/:userId/:userName/:userPwd", func(ctx *gin.Context) {
			tempId := ctx.Param("userId")
			userId, err := strconv.ParseUint(tempId, 10, 64)
			if err != nil {
				ctx.JSON(
					http.StatusNotAcceptable, gin.H{
						"errMsg": "userId parseErr",
					})
				return
			}
			userName := ctx.Param("userName")
			userPwd := ctx.Param("userPwd")
			newuser, err := model.MyUserDao.Register(uint(userId), userName, userPwd)
			if err != nil {
				ctx.JSON(
					http.StatusNotAcceptable, gin.H{
						"errMsg": "Register Fail: " + err.Error(),
					})
				return
			}
			ctx.JSON(
				http.StatusOK, gin.H{
					"success": newuser,
				})
		})

		// 路由格式: "http://localhost:8005/v1/users?username=Jack"
		router.GET("/v1/users", func(ctx *gin.Context) {
			queryuser := ctx.DefaultQuery("username", "")
			if queryuser == "" {
				users, err := model.MyUserDao.GetAllUsers()
				if err != nil {
					ctx.String(http.StatusNotAcceptable, "errMsg = "+err.Error())
					return
				}
				// ctx.String(200, fmt.Sprintf("%v", users))
				ctx.JSON(200, users)
				return
			} else {
				user, err := model.MyUserDao.GetUserByUserName(queryuser)
				if err != nil {
					ctx.String(http.StatusNotAcceptable, "errMsg = "+err.Error())
					return
				}
				ctx.JSON(
					200, gin.H{
						"success": user,
					})
			}
		})
	}

	// 用v2分組，繼承v2的路由
	v2 := router.Group("/v2")
	{
		// 這裡路由是""，前面加上v2的路由，最終路由 = "/v2" + "/something"
		// 特別的是"http://localhost:8005/v2/users"，不會路由到這(到下面那個)，而且無任何編譯錯誤，此路由正常使用
		// 路由格式: "http://localhost:8005/v2/anything(except "users")"
		v2.GET("/:test", service.JustTest)

		// 這裡路由是""，前面加上v2的路由，最終路由 = "/v2" + "/users"
		// 路由格式: "http://localhost:8005/v2/users?username=Jack" or "http://localhost:8005/v2/users"
		v2.GET("/users", service.GetUsers)

		v2.Use(service.MustLogin()) // 自此之後定義的所有v2路由都需要先 MustLogin()
		{
			// 路由格式: "http://localhost:8005/v2/user?token=xxx&username=xxx&pwd=xxx&userid=xxx"
			v2.POST("/user", service.AddUser) // 需登入

			// 路由格式: "http://localhost:8005/v2/muser?token=xxx&username=xxx&pwd=xxx&userid=xxx"
			v2.POST("/muser", service.AddMUser) // 需登入

			// 路由格式: "http://localhost:8005/v2/user?token=xxx&username=Jack"
			v2.DELETE("/user", service.DelUser) // 需登入
		}

	}

	router.Run(":8005")
}
