FROM golang:latest
WORKDIR /GoGin
COPY . .
RUN go mod download
RUN go build -o main ./main/.
EXPOSE 8005
CMD ["./main/main"]